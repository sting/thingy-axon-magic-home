# Magic Home LED controller firmware #

This repository contains firmware for [Magic Home LED Controller](https://www.google.ch/?q=magic+home+led+controller) devices. Firmware is customized for usage with [Thingy](http://thingy.io) system.

## Supported devices ##

* LED Controller (RGB)

![](extras/magic_home_pcb.jpg)

## Home Assistant Integration ##
In order to integrate the device with [Home Assistant](https://home-assistant.io/) system it is necessery to configure **MQTT Component** in Home Assistant configuration and add the following **MQTT Light component**:

```
light:
  - platform: mqtt
    command_topic: "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'Command' stream
    state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'State' stream
    brightness_command_topic: "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'Command' stream
    brightness_state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'State' stream
    brightness_scale: 100
    rgb_command_topic: "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'Command' stream
    rgb_state_topic:   "brain/<Brain_UiD>/axon/<Axon_UiD>/stream/<Stream_UiD>" // 'State' stream
```

## Additional info and documentation

Additional pictures you can find [here](extras/).

More information about the **Thingy** system you can under [http://thingy.io](http://thingy.io) and detailed documentation under [http://docs.thingy.io](http://docs.thingy.io).

## Credits & licenses

Copyright (c) 2017 Thingy.IO
