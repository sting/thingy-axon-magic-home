/*
  Thingy Axon - Magic Home LED Controller

  This axon runs on Magic Home LED Controller (https://www.google.ch/?q=magic+home+led+controller) and connects to Thingy system

  Copyright (c) 2017 Thingy.IO
*/

#define   PIN_LED_R   14
#define   PIN_LED_G   5
#define   PIN_LED_B   12
//#define   PIN_LED_W   13

#include <Thingy.h>
#include <Ticker.h>

Thingy::Axon led;
Ticker ticker;

bool state = false;
unsigned short red = 0, green = 0, blue = 0, brightness = 0;

bool _state = false;
unsigned short _red = 0, _green = 0, _blue = 0, _brightness = 0;

bool load_state()
{
  File stateFile = SPIFFS.open("/state.json", "r");
  if (!stateFile)
  {
    Serial << F("Failed to open state file  ");
    return false;
  }

  size_t size = stateFile.size();
  if (size > 255)
  {
    Serial << F("State file size is too large ");
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  stateFile.readBytes(buf.get(), size);

  StaticJsonBuffer<255> settingsBuffer;
  JsonObject& stateObject = settingsBuffer.parseObject(buf.get());

  stateFile.close();

  if (!stateObject.success())
  {
    Serial << F("Failed to parse State file ");
    return false;
  }

  state      = stateObject.get<bool>("S");
  red        = stateObject.get<unsigned short>("R");
  green      = stateObject.get<unsigned short>("G");
  blue       = stateObject.get<unsigned short>("B");
  brightness = stateObject.get<unsigned short>("L");

  return true;
}

bool save_state()
{
  StaticJsonBuffer<255> settingsBuffer;
  JsonObject& stateObject = settingsBuffer.createObject();

  stateObject["S"] = state;
  stateObject["R"] = red;
  stateObject["G"] = green;
  stateObject["B"] = blue;
  stateObject["L"] = brightness;

  File stateFile = SPIFFS.open("/state.json", "w+");
  if (!stateFile)
  {
    LOG << F("Failed to open state file for writing ");
    return false;
  }

  stateObject.printTo(stateFile);
  stateFile.close();

  return true;
}

void tick()
{
  // hooking animation
}

void set_led()
{
  analogWrite(PIN_LED_R, state ? 0.01 * brightness * red : 0);
  analogWrite(PIN_LED_G, state ? 0.01 * brightness * green : 0);
  analogWrite(PIN_LED_B, state ? 0.01 * brightness * blue : 0);
  Serial << (state ? "ON ":"OFF") << " -" << " R:" << red << " G:" << green << " B:" << blue << " - " << brightness << "%" << endl;
}

void set_state(bool s)
{
  state = s;
  save_state();
  set_led();
  led["State"].put(s ? "ON" : "OFF", 1, true);
}

void set_rgb(unsigned short r, unsigned short g, unsigned short b)
{
  red = r; green = g; blue = b;
  save_state();
  set_led();
  led["Color State"].put(String(r)+","+String(g)+","+String(b), 1, true);
}

void set_brightness(unsigned short b)
{
  brightness = b;
  save_state();
  set_led();
  led["Brightness State"].put(String(b), 1, true);
}

void handle_state(const Thingy::Stream& stream, const String& value)
{
  if (value == "ON")
    _state = true;
  if (value == "OFF")
    _state = false;
}

void handle_color(const Thingy::Stream& stream, const String& value)
{
  DynamicJsonBuffer payloadBuffer(64);
  JsonArray& colorObject = payloadBuffer.parseArray("["+value+"]");
  if (!colorObject.success()) return;
  if (colorObject.size() != 3) return;

  _red = colorObject.get<unsigned short>(0);
  _green = colorObject.get<unsigned short>(1);
  _blue = colorObject.get<unsigned short>(2);
}

void handle_brighness(const Thingy::Stream& stream, const String& value)
{
  long b = value.toInt();

  if (b < 0) return;
  if (b > 100) return;

  _brightness = b;
}

void setup()
{
	// Setup console
	Serial.begin(115200);
	delay(10);

	Serial << endl << endl  << F("MagicHome.LED") << " - " << __DATE__ << " " << __TIME__ << endl << "#" << endl << endl;

  // LEDs
  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_G, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
  analogWriteRange(255);

  ticker.attach(0.6, tick);

  // Thingy
  led.setup();

  // Persisted state
  load_state();
  _state = state; _red = red; _green = green; _blue = blue; _brightness = brightness;
  set_led();

  if (!led.begin())
  {
    Serial << F("MagicHome.LED - Fatal error. Please restart.") << endl << endl << endl;
    return;
  }

  ticker.detach();

	// setting handles
  led["Command"].setHandle(handle_state);
  led["Brightness Command"].setHandle(handle_brighness);
  led["Color Command"].setHandle(handle_color);
}

void loop()
{
  led.loop();

  if (state != _state)
  {
    set_state(_state);
    _state = state;
  }
  if (red != _red || green != _green || blue != _blue)
  {
    set_rgb(_red, _green, _blue);
    _red = red; _green = green; _blue = blue;
  }
  if (brightness != _brightness)
  {
    set_brightness(_brightness);
    _brightness = brightness;
  }
}
